enum SpiralDirectionEnum
    {
    CLOCKWISE = 'clockwise',
    COUNTER_CLOCKWISE = 'counterClockwise'
}

export default SpiralDirectionEnum;