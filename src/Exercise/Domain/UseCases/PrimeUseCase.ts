import PrimesRepPayload from '../../InterfaceAdapters/Payloads/PrimesRepPayload';

class PrimesUseCase
{
    async handle(payload: PrimesRepPayload): Promise<any>
    {
        return {
            result: this.printNPrimes(payload.getQty())
        };
    }

    printNPrimes(qty: number)
    {
        let i = 1;
        const primes = [];

        while (primes.length < qty)
        {
            let isPrime = true;
            i++;

            for (let j = 2; j <= i / 2; ++j)
            {
                if (i % j === 0)
                {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime)
            {
                primes.push(i);
            }
        }

        return primes;
    }

}

export default PrimesUseCase;