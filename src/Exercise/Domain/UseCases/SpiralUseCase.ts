import SpiralDirectionEnum from '../Enums/SpiralDirectionEnum';
import SpiralRepPayload from '../../InterfaceAdapters/Payloads/SpiralRepPayload';
import ErrorHttpException from '../../../App/Presentation/Shared/ErrorHttpException';
import { StatusCode } from '@digichanges/shared-experience';

interface SpiralConfig
{
    iRow: number;
    iColumn: number;
    rows: number;
    columns: number;
    matrix: number[][];
}

class SpiralUseCase
{
    async handle(payload: SpiralRepPayload): Promise<any>
    {
        return {
            result: this.printMatrix(payload.getSize(), payload.getMatrix(), payload.getSpiralDirection())
        };
    }

    /*
      iRow - índice de fila inicial
      filas - índice de fila final
      iColum - índice de columna inicial
      column - índice de la columna final
      i - iterador
      */
    private printMatrix(size: number, matrix: number[][], direction: SpiralDirectionEnum): any
    {
        if (matrix.length !== size || !matrix.every((row) => row.length === size))
        {
            throw new ErrorHttpException(StatusCode.HTTP_BAD_REQUEST, `Debes definir una matriz cuadrada ${size}x${size}`, []);
        }

        const config = {
            iRow: 0,
            iColumn: 0,
            rows: matrix.length,
            columns: matrix[0].length,
            matrix
        };

        const spiralFactory = {
            clockwise: SpiralUseCase.clockwise,
            counterClockwise: SpiralUseCase.counterClockwise
        };

        return {
            direction,
            size,
            matrix,
            output: spiralFactory[direction](config)
        };
    }

    private static clockwise(config: SpiralConfig): number[]
    {
        let i;
        let {
            iRow,
            iColumn,
            rows,
            columns
        } = config;

        const { matrix } = config;

        const out: number[] = [];

        while (iRow < rows && iColumn < columns)
        {
            // imprime la primera fila de las filas restantes
            for (i = iColumn; i < columns; ++i)
            {
                out.push(matrix[iRow][i]);
            }
            iRow++;

            // imprime la última columna de las columnas restantes
            for (i = iRow; i < rows; ++i)
            {
                out.push(matrix[i][columns - 1]);
            }
            columns--;


            // imprime la última fila de las filas restantes
            if (iRow < rows)
            {
                for (i = columns - 1; i >= iColumn; --i)
                {
                    out.push(matrix[rows - 1][i]);
                }
                rows--;
            }

            // imprime la primera columna de las columnas restantes
            if (iColumn < columns)
            {
                for (i = rows - 1; i >= iRow; --i)
                {
                    out.push(matrix[i][iColumn]);
                }
                iColumn++;
            }

        }

        return out;
    }

    private static counterClockwise(config: SpiralConfig):number[]
    {
        let i;
        let cnt = 0;
        let {
            iRow,
            iColumn,
            rows,
            columns
        } = config;

        const { matrix } = config;

        const out: number[] = [];

        const total = rows * columns;

        while (iRow < rows && iColumn < columns)
        {
            if (cnt === total)
            {
                break;
            }

            // Imprime la primera columna de las columnas restantes
            for (i = iRow; i < rows; ++i)
            {
                out.push(matrix[i][iColumn]);
                cnt++;
            }
            iColumn++;

            if (cnt === total)
            {
                break;
            }

            // Imprime la última fila de las filas restantes
            for (i = iColumn; i < columns; ++i)
            {
                out.push(matrix[rows - 1][i]);
                cnt++;
            }
            rows--;

            if (cnt === total)
            {
                break;
            }

            // Print the last column from the remaining columns
            if (iRow < rows)
            {
                for (i = rows - 1; i >= iRow; --i)
                {
                    out.push(matrix[i][columns - 1]);
                    cnt++;
                }
                columns--;
            }

            if (cnt === total)
            {
                break;
            }

            // Imprime la primera fila de las filas restantes
            if (iColumn < columns)
            {
                for (i = columns - 1; i >= iColumn; --i)
                {
                    out.push(matrix[iRow][i]);
                    cnt++;
                }
                iRow++;
            }
        }

        return out;
    }
}

export default SpiralUseCase;
