import SpiralDirectionEnum from '../../Domain/Enums/SpiralDirectionEnum';

interface SpiralRepPayload
{
    getSpiralDirection(): SpiralDirectionEnum;
    getMatrix(): number[][];
    getSize(): number;
}

export default SpiralRepPayload;
