interface PrimesRepPayload
{
    getQty(): number;
}

export default PrimesRepPayload;