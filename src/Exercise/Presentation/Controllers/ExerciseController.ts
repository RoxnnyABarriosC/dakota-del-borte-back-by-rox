import ValidatorRequest from '../../../App/Presentation/Shared/ValidatorRequest';
import SpiralRepPayload from '../../InterfaceAdapters/Payloads/SpiralRepPayload';
import SpiralUseCase from '../../Domain/UseCases/SpiralUseCase';
import CirclesRepPayload from '../../InterfaceAdapters/Payloads/CirclesRepPayload';
import CircleUseCase from '../../Domain/UseCases/CircleUseCase';
import PrimesRepPayload from '../../InterfaceAdapters/Payloads/PrimesRepPayload';
import PrimesUseCase from '../../Domain/UseCases/PrimeUseCase';

class ExerciseController
{
    public async spiral(request: SpiralRepPayload)
    {
        await ValidatorRequest.handle(request);

        const useCase = new SpiralUseCase();
        return await useCase.handle(request);
    }

    public async circles(request: CirclesRepPayload)
    {
        await ValidatorRequest.handle(request);

        const useCase = new CircleUseCase();
        return await useCase.handle(request);
    }

    public async primes(request: PrimesRepPayload)
    {
        await ValidatorRequest.handle(request);

        const useCase = new PrimesUseCase();
        return await useCase.handle(request);
    }
}

export default ExerciseController;
