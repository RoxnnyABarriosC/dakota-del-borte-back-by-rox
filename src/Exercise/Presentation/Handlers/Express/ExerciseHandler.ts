import { NextFunction, Request, Response } from 'express';
import { inject } from 'inversify';
import { controller, httpPost, next, request, response } from 'inversify-express-utils';
import { StatusCode } from '@digichanges/shared-experience';

import { TYPES } from '../../../../types';
import Responder from '../../../../App/Presentation/Shared/Express/Responder';
import SpiralRepRequest from '../../Requests/SpiralRepRequest';

import ExerciseController from '../../Controllers/ExerciseController';
import CirclesRepRequest from '../../Requests/CirclesRepRequest';
import PrimesRepRequest from '../../Requests/PrimesRepRequest';

@controller('/api/exercises')
class ExerciseHandler
{
    @inject(TYPES.Responder)
    private responder: Responder;
    private readonly controller: ExerciseController;

    constructor()
    {
        this.controller = new ExerciseController();
    }

    @httpPost('/spiral')
    public async spiral(@request() req: Request, @response() res: Response, @next() nex: NextFunction)
    {
        const _request = new SpiralRepRequest(req.body);

        const data = await this.controller.spiral(_request);

        this.responder.send(data, req, res, StatusCode.HTTP_OK);
    }

    @httpPost('/circles')
    public async circles(@request() req: Request, @response() res: Response, @next() nex: NextFunction)
    {
        const _request = new CirclesRepRequest(req.body);

        const data = await this.controller.circles(_request);

        this.responder.send(data, req, res, StatusCode.HTTP_OK);
    }

    @httpPost('/primes')
    public async primes(@request() req: Request, @response() res: Response, @next() nex: NextFunction)
    {
        const _request = new PrimesRepRequest(req.body);

        const data = await this.controller.primes(_request);

        this.responder.send(data, req, res, StatusCode.HTTP_OK);
    }
}

export default ExerciseHandler;
