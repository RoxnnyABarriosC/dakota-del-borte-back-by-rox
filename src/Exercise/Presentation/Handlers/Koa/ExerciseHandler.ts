import Koa from 'koa';
import Router from 'koa-router';
import { StatusCode } from '@digichanges/shared-experience';
import Responder from '../../../../App/Presentation/Shared/Koa/Responder';
import ExerciseController from '../../Controllers/ExerciseController';
import SpiralRepRequest from '../../Requests/SpiralRepRequest';
import CirclesRepRequest from '../../Requests/CirclesRepRequest';
import PrimesRepRequest from '../../Requests/PrimesRepRequest';

const routerOpts: Router.IRouterOptions = {
    prefix: '/api/exercises'
};

const ExerciseHandler: Router = new Router(routerOpts);
const responder: Responder = new Responder();
const controller: ExerciseController = new ExerciseController();

ExerciseHandler.post('/spiral', async(ctx: Koa.ParameterizedContext & any) =>
{
    const request = new SpiralRepRequest(ctx.request.body);

    const data = await controller.spiral(request);

    responder.send(data, ctx, StatusCode.HTTP_OK);
});

ExerciseHandler.post('/circles', async(ctx: Koa.ParameterizedContext & any) =>
{
    const request = new CirclesRepRequest(ctx.request.body);

    const data = await controller.circles(request);

    responder.send(data, ctx, StatusCode.HTTP_OK);
});

ExerciseHandler.post('/primes', async(ctx: Koa.ParameterizedContext & any) =>
{
    const request = new PrimesRepRequest(ctx.request.body);

    const data = await controller.primes(request);

    responder.send(data, ctx, StatusCode.HTTP_OK);
});

export default ExerciseHandler;
