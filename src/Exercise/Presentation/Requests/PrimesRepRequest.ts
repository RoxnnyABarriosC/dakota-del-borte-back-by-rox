import { IsNumber, Max, Min } from 'class-validator';
import PrimesRepPayload from '../../InterfaceAdapters/Payloads/PrimesRepPayload';

class PrimesRepRequest implements PrimesRepPayload
{
    @IsNumber()
    @Min(1)
    @Max(100)
    protected qty: number;

    constructor(data: Record<string, any>)
    {
        this.qty = data.qty;
    }

    getQty(): number
    {
        return this.qty;
    }
}

export default PrimesRepRequest;
