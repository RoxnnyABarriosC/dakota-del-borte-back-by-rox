import SpiralRepPayload from '../../InterfaceAdapters/Payloads/SpiralRepPayload';
import { IsArray, IsEnum, IsNumber, Max, Min } from 'class-validator';
import SpiralDirectionEnum from '../../Domain/Enums/SpiralDirectionEnum';

class SpiralRepRequest implements SpiralRepPayload
{
    @IsEnum(SpiralDirectionEnum)
    protected spiralDirection: SpiralDirectionEnum;

    @IsNumber()
    @Min(1)
    @Max(10)
    protected size: number;

    @IsArray()
    protected matrix: number[][];

    constructor(data: Record<string, any>)
    {
        this.spiralDirection = data.spiralDirection;
        this.size = data.size;
        this.matrix = data.matrix;
    }

    getSpiralDirection(): SpiralDirectionEnum
    {
        return this.spiralDirection;
    }

    getSize(): number
    {
        return this.size;
    }

    getMatrix(): number[][]
    {
        return this.matrix;
    }
}

export default SpiralRepRequest;
